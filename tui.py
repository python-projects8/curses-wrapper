from cgitb import handler
import curses
from curses import wrapper
from typing import List
from widgets import ListBox, ProgressListBox, KeyMap
from random import randint

def main(stdscr):
    plb = ProgressListBox(['Apples', 'Oranges', 'Pears', 'Mangos'], 0, 0, 25)
    keymap = KeyMap()
    stdscr.clear()
    stdscr.refresh()

    plb.register_keymap(keymap)
    keymap.register('p', 'Add tomatoes')
    keymap.register('d', 'Delete item')
    keymap.register('f', 'Failed random item')
    keymap.register('s', 'Succeed random item')

    plb.draw_on_screen()
    keymap.draw_on_screen()

    while True:
        key = stdscr.getkey()

        if key == 'q':
            exit(0)
        else:
            plb.handle_input(key)

if __name__ == '__main__':
    wrapper(main)