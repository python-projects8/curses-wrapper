import curses

class Widget:

    def __init__(self, pos_x:int, pos_y:int):
        self.x = pos_x
        self.y = pos_y

    def handle_input(self, key:str):
        pass

    def draw_on_screen(self):
        pass

    def get_keymap(self):
        pass

class ListBox(Widget):

    def __init__(self, items:list, pos_x:int, pos_y:int, width:int, handlers:dict={}):
        super().__init__(pos_x, pos_y)
        self.width = width
        self.height = len(items)
        self.cursor = 0
        self.pad_index = 0
        self.items = items
        self.selection = []
        self.handlers = handlers

        curses.init_pair(1, curses.COLOR_BLUE, curses.COLOR_YELLOW)
        curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_WHITE)
        curses.init_pair(3, curses.COLOR_BLUE, curses.COLOR_WHITE)
        self.CP_SELECTION = curses.color_pair(1)
        self.CP_CURSOR = curses.color_pair(2)
        self.CP_CURSORS_AND_SELECTION = curses.color_pair(3)
        
    def _move_cursor(self, index):

        if index < 0:
            index = 0

        if index >= len(self.items):
            index = len(self.items) - 1
        
        self.cursor = index

    def handle_input(self, key:str):
        
        if key == 'KEY_DOWN' or key == 'j':
            self._move_cursor(self.cursor + 1)
        elif key == 'KEY_UP' or key == 'k':
            self._move_cursor(self.cursor - 1)
        elif key == '\n' or key == 'l':
            if self.items[self.cursor] in self.selection:
                self.selection.remove(self.items[self.cursor])
            else:
                self.selection.append(self.items[self.cursor])
        elif key == 'a':
            self.add_selection(items=self.items)
        elif key == 'c':
            self.clear_selection()
        elif key in self.handlers:
            self.handlers[key](self)
        
        self._quick_refresh()

    def _quick_refresh(self):

        for i, item in enumerate(self.items):

            if item in self.selection:
                if i == self.cursor:
                    self.pad.addstr(i, 0, item, self.CP_CURSORS_AND_SELECTION)
                else:
                    self.pad.addstr(i, 0, item, self.CP_SELECTION)
            elif i == self.cursor:
                self.pad.addstr(i, 0, item, self.CP_CURSOR)
            else:
                self.pad.addstr(i, 0, item)


        self.pad.refresh(self.pad_index, 0, self.y, self.x, self.y + self.height - 1, self.x + self.width - 1)

    def _refresh(self):
        self.pad.clear()
        for i, item in enumerate(self.items):
            if item in self.selection:
                if i == self.cursor:
                    self.pad.addstr(i, 0, item, self.CP_CURSORS_AND_SELECTION)
                else:
                    self.pad.addstr(i, 0, item, self.CP_SELECTION)
            elif i == self.cursor:
                self.pad.addstr(i, 0, item, self.CP_CURSOR)
            else:
                self.pad.addstr(i, 0, item)
        
        self.pad.refresh(self.pad_index, 0, self.y, self.x, self.y + self.height - 1, self.x + self.width - 1)

    def draw_on_screen(self):
        self.pad = curses.newpad(self.height, self.width)
        self._refresh()
    
    def add_selection(self, index:int=-1, item:str=None, items:list=[]):
        if index != -1:
            self.selection.append(self.items[index])
        elif item and item in self.items:
            self.selection.append(item)
        elif items:
            self.selection += items

        self.selection = list(set(self.selection))
        
        self._quick_refresh()

    def remove_selection(self, index:int=-1, item:str=None, items:list=[]):
        if index != -1:
            self.selection.remove(self.items[index])
        elif item and item in self.items:
            self.selection.remove(item)
        elif items:
            for item in items:
                self.selection.remove(item)
        
        self._quick_refresh()

    def clear_selection(self):
        self.selection = []

        self._quick_refresh()

    def add_items(self, index:int=-1, item:str=None, items:list=[]):
        if index != -1:
            self.items.append(self.items[index])
        elif item:
            self.items.append(item)
        elif items:
            self.items += items

        self.items = list(set(self.items))
        self.height = len(self.items)
        self.pad.resize(self.height, self.width)

        self._quick_refresh()
    
    def remove_items(self, index:int=-1, item:str=None, items:list=[]):
        if index != -1:
            self.items.remove(self.items[index])
        elif item:
            self.items.remove(item)
        elif items:
            for item in items:
                self.items.remove(item)

        self._refresh()

    def register_keymap(self, keymap):
        keymap.register('UP', 'Move cursor up')
        keymap.register('k', 'Move cursor up')
        keymap.register('DOWN', 'Move cursor down')
        keymap.register('j', 'Move cursor down')
        keymap.register('ENTER', 'Select item at cursor')
        keymap.register('l', 'Select item at cursor')
        keymap.register('c', 'Clear selection')
        keymap.register('a', 'Select all items')

class ProgressListBox(ListBox):

    def __init__(self, items: list, pos_x: int, pos_y: int, width: int, handlers: dict = {}):
        super().__init__(items, pos_x, pos_y, width, handlers)
        
        curses.init_pair(4, curses.COLOR_RED, curses.COLOR_BLACK)
        curses.init_pair(5, curses.COLOR_GREEN, curses.COLOR_BLACK)
        curses.init_pair(6, curses.COLOR_YELLOW, curses.COLOR_BLACK)

        self.CP_IN_PROGRESS = curses.color_pair(6)
        self.CP_SUCCESSFUL = curses.color_pair(5)
        self.CP_FAILED = curses.color_pair(4)

        self.successful = []
        self.failed = []

    def _quick_refresh(self):
        for i, item in enumerate(self.items):
            if i == self.cursor:
                self.pad.addstr(i, 0, item, self.CP_SELECTION)
            elif item in self.successful:
                self.pad.addstr(i, 0, item, self.CP_SUCCESSFUL)
            elif item in self.failed:
                self.pad.addstr(i, 0, item, self.CP_FAILED)
            else:
                self.pad.addstr(i, 0, item, self.CP_IN_PROGRESS)

        self.pad.refresh(self.pad_index, 0, self.y, self.x, self.y + self.height - 1, self.x + self.width - 1)

    def add_successful_item(self, item:str):
        self.successful.append(item)
        self._quick_refresh()

    def add_failed_item(self, item:str):
        self.failed.append(item)
        self._quick_refresh()

    def _refresh(self):
        self.pad.clear()
        self._quick_refresh()
    
    def register_keymap(self, keymap):
        keymap.register('UP', 'Move cursor up')
        keymap.register('k', 'Move cursor up')
        keymap.register('DOWN', 'Move cursor down')
        keymap.register('j', 'Move cursor down')

class KeyMap(Widget):

    HEIGHT_PERCENT = 0.1

    def __init__(self):
        self.height = int(curses.LINES*self.HEIGHT_PERCENT)
        self.width = curses.COLS
        super().__init__(0, curses.LINES - self.height - 1)
        self.win = curses.newwin(self.height, self.width, self.y, self.x)
        self.keymap = []

    def register(self, key:str, name:str, description:str=None):
        for item in self.keymap:
            if item['name'] == name:
                item['key'] += '/' + key
                break
        else:
            self.keymap.append({
                'key': key, 
                'name': name,
                'description': description or name
            })
    
    def unregister(self, key):
        for i, item in enumerate(self.keymap):
            if item['key'] == key:
                del self.keymap[i]
                break
            elif key in item['key'].split('/'):
                k = item['key'].split('/')
                k.remove(key)
                item['key'] = '/'.join(k)
        
        self.refresh()
    
    def refresh(self):
        self.win.clear()
        self.draw_on_screen()

    def draw_on_screen(self):
        cursor_x = 0
        cursor_y = 0

        for map in self.keymap:
            text = f'[{map["key"]}] {map["name"]}'

            if cursor_x + len(text) >= self.width:
                cursor_x = 0
                cursor_y += 1
            
            if cursor_y >= self.height:
                break

            self.win.addstr(cursor_y, cursor_x, text)
            cursor_x += len(text) + 2

        self.win.refresh()